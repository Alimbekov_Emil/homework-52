import React from 'react';
import './App.css';
import Card from './components/Card/Card';
import CardDeck from './components/CardDeck/CardDeck';
import PokerHand from './components/PokerHand/PokerHand';

class App extends React.Component {
  state = {
    cards: [],
    combination: 'Попытай Удачу!',
  };

  getCards = () => {
    const cardDeck = new CardDeck();
    const cards = cardDeck.getCards(5);
    this.setState({
      cards: cards
    }, () => this.GetOutCome());

  };

  GetOutCome = () =>{
    const pokerHand = new PokerHand();
    const combination = pokerHand.getCombination(this.state.cards);
    this.setState({
      combination: combination,
    })
  };


  render() {
    return (
       <div className='card-block'>
           <div >
             <button onClick={this.getCards} className="btn">Попробывать!</button>
           </div>
           <div>
             <Card cards={this.state.cards}></Card>
           </div>
            <b className='text-combination'>{this.state.combination}</b>
       </div>
    );
  };
};

export default App;
