import React from 'react';
import './Card.css';

const Card = (props) =>{
  const suits = {
    'hearts' : '♥' ,
    'diams' : '♦',
    'spades' : '♠',
    'clubs' : '♣',
  };

  return(
    props.cards.map(card => {
      return (
        <div className={'Card Card-rank-' + card.rank  + ' Card-' + card.suit}>
          <span className='Card-rank'>{card.rank.toUpperCase()}</span>
          <span className='Card-suit'>{suits[card.suit]}</span>
        </div>
      )
    })
  )
};

export default Card;