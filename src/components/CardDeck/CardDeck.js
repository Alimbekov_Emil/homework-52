class CardDeck {
  constructor() {
    this.suit = ['spades', 'diams', 'hearts', 'clubs'];
     this.rank = [
      '2',
      '3',
      '4',
      '5',
      '6',
      '7',
      '8',
      '9',
      '10',
      'j',
      'q',
      'k',
      'a'
    ];

     this.allCards = [];

     for(let i = 0; i < this.rank.length; i++) {
       for(let j = 0; j < this.suit.length; j ++) {
         const myCard = {rank: this.rank[i], suit: this.suit[j]};
         this.allCards.push(myCard);
       }
     }
  }

  getCard() {
    const random = Math.floor(Math.random() * this.allCards.length);
    const randCard = this.allCards[random];
    this.allCards.splice(random, 1);
    return randCard;
  }

  
  getCards(howMany){
    const array = [];
    for (let i = 0; i < howMany; i++){
      array.push(this.getCard());
    }
    return array;
  }
}

export default CardDeck;